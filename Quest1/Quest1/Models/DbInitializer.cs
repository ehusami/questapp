﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Models
{
    public class DbInitializer
    {
        public static void Initialize(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                QuestContext context = serviceScope.ServiceProvider.GetService<QuestContext>();



                // Look for any patient.
                if (context.Booking.Any())
                {
                    return;   // DB has been seeded
                }

                /*******************************/
                var lUTimeSlots = new LUTimeSlot[]
              {
                new LUTimeSlot { TimeSlotTitle =9},
                new LUTimeSlot { TimeSlotTitle =10},
                new LUTimeSlot { TimeSlotTitle =11},
                new LUTimeSlot { TimeSlotTitle =12},
                new LUTimeSlot { TimeSlotTitle =1},
                new LUTimeSlot { TimeSlotTitle =2},
                new LUTimeSlot { TimeSlotTitle =3},
                new LUTimeSlot { TimeSlotTitle =4},
                new LUTimeSlot { TimeSlotTitle =5}
              };
                foreach (LUTimeSlot t in lUTimeSlots)
                {
                    context.LUTimeSlot.Add(t);
                }
                context.SaveChanges();

                /**************************************/

                var lUTreatments = new LUTreatment[]
              {
                new LUTreatment { TreatmentTitle ="treatment 1", TreatmentDescription="Description1"},
                new LUTreatment { TreatmentTitle ="treatment 2", TreatmentDescription = "Description2" },
                new LUTreatment { TreatmentTitle ="treatment 3", TreatmentDescription = "Description3" }
              };
                foreach (LUTreatment r in lUTreatments)
                {
                    context.LUTreatment.Add(r);
                }
                context.SaveChanges();

                /**************************************/

                var bookings = new Booking[]
               {
            new Booking {Name="esraa",Email="esraa.hus93@yahoo.com",Mobile="0777768867",TreatmentId=lUTreatments.Single( r => r.TreatmentTitle == "treatment 1").ID ,TimeSlotId=lUTimeSlots.Single(t=>t.TimeSlotTitle==1).ID,Note="NOTE1",BookedDate=DateTime.Parse("2010-09-01"),IsDeleted=false},
            new Booking {Name="esraa1",Email="esraa.hus93@gmail.com",Mobile="0777768867",TreatmentId=lUTreatments.Single( r => r.TreatmentTitle =="treatment 2").ID,TimeSlotId=lUTimeSlots.Single(t=>t.TimeSlotTitle==2).ID,Note="NOTE1",BookedDate=DateTime.Parse("2010-09-01"),IsDeleted=false}
               };
                foreach (Booking s in bookings)
                {
                    var BookingDataBase = context.Booking.Where(
                        d =>
                                d.LUTimeSlot.ID == s.TimeSlotId &&
                                d.LUTreatment.ID == s.TreatmentId).SingleOrDefault();
                    if (BookingDataBase == null)
                    {
                        context.Booking.Add(s);
                    }
                }
                context.SaveChanges();




            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Models
{
    public class LUTimeSlot
    {
        [Key]
        public int ID { get; set; }
        public int TimeSlotTitle { get; set; }

        public ICollection<Booking> Booking { get; set; }
    }
}

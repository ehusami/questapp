﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Models
{
    public class QuestContext : DbContext
    {
        public QuestContext()
        {
        }

        public QuestContext(DbContextOptions<QuestContext> options) : base(options)
        {
        }

        public DbSet<Booking> Booking { get; set; }
        public DbSet<LUTimeSlot> LUTimeSlot { get; set; }
        public DbSet<LUTreatment> LUTreatment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Booking>().ToTable("Booking");
            modelBuilder.Entity<LUTimeSlot>().ToTable("LUTimeSlot");
            modelBuilder.Entity<LUTreatment>().ToTable("LUTreatment");
            modelBuilder.Entity<Booking>(
                entity =>
                {
                    entity.HasOne(e => e.LUTimeSlot).WithMany(e => e.Booking).HasForeignKey(e => e.TimeSlotId).IsRequired();
                    entity.HasOne(e => e.LUTreatment).WithMany(e => e.Booking).HasForeignKey(e => e.TreatmentId).IsRequired();
                });
            modelBuilder.Entity<LUTimeSlot>(entity =>
            {
                entity.HasKey(t => t.ID);
            });
            modelBuilder.Entity<LUTreatment>(entity =>
            {
                entity.HasKey(t => t.ID);
            });
        }
    }
}

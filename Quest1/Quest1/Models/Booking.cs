﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Models
{
    public class Booking
    {
        public int BookingId { get; set; }

        [StringLength(50, MinimumLength = 3)]
        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid Email Address.")]
        public string Email { get; set; }

        [Required]
        public string Mobile { get; set; }
        public int TreatmentId { get; set; }
        public int TimeSlotId { get; set; }
        public string Note { get; set; }

        [Display(Name = "BookedDate")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime BookedDate { get; set; }
        public bool IsDeleted { get; set; }
        public LUTimeSlot LUTimeSlot { get; set; }
        public LUTreatment LUTreatment { get; set; }

    }
}

﻿using Quest1.Data.Interfaces;
using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Repositories
{
    public class LUTreatmentRepository : ILUTreatmentRepository
    {
        private readonly QuestContext _questContext;
        public LUTreatmentRepository (QuestContext questContext)
        {
            _questContext = questContext;
        }

        public IEnumerable<LUTreatment> LUTreatments => _questContext.LUTreatment;
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Quest1.Data.Interfaces;
using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Repositories
{
    public class BookingRepository : IBookingRepository
    {
        private readonly QuestContext _questContext;
        public BookingRepository(QuestContext questContext)
        {
            _questContext = questContext;
        }
        public IEnumerable<Booking> Bookings => _questContext.Booking.Include(c => c.TreatmentId).Include(c => c.TimeSlotId);

        public Booking GetBookingById(int BookingId) => _questContext.Booking.FirstOrDefault(p => p.BookingId == BookingId);
    }
}

﻿using Quest1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quest1.Data.Interfaces
{
    public interface IBookingRepository
    {
        IEnumerable<Booking> Bookings { get;}
        Booking GetBookingById(int BookingId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Quest1.Models;

namespace Quest1.Pages.test
{
    public class CreateModel : PageModel
    {
        private readonly Quest1.Models.QuestContext _context;


        public CreateModel(Quest1.Models.QuestContext context )
        {
            _context = context;
          

        }

        public IActionResult OnGet()
        {
            ViewData["TimeSlotId"] = new SelectList(_context.LUTimeSlot, "ID", "TimeSlotTitle");
            ViewData["TreatmentId"] = new SelectList(_context.LUTreatment, "ID", "TreatmentTitle");
            return Page();
        }

        [BindProperty]
        public Booking Booking { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (!String.IsNullOrWhiteSpace( _context.Booking.Where(s=>s.Email==Booking.Email).ToString()))
            {
                _context.Booking.Add(Booking);
                await _context.SaveChangesAsync();
            }
           
            return RedirectToPage("./Index");
        }
    }
}
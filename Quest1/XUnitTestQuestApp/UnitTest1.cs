using System;
using Xunit;
using Quest1.Models;
using Quest1.Pages.test;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Linq;

namespace XUnitTestQuestApp.Models
{
    public class UnitTest1
    {
        [Fact]
        public void TestBookModel()
        {
            CheckPropertyValidation cpv = new CheckPropertyValidation();
            var book = new Booking
            {
                Name = "esraa",
                Email = "esraa.hus@yahoo.com",
                Mobile = "0777768867",
                TreatmentId = 1,
                TimeSlotId = 1,
                Note = "note1",
                BookedDate = DateTime.Parse("2010-09-01"),
                IsDeleted = false
            };
            var errorcount = cpv.myValidation(book).Count();
            Xunit.Assert.Equal(0, errorcount);


        }
        //should failed 
        [Fact]
        public void TestBookModel_Withinvaildemail()
        {
            CheckPropertyValidation cpv = new CheckPropertyValidation();
            var book = new Booking
            {
                Name = "esraa",
                Email = "esraa.husyahoo.com",
                Mobile = "0777768867",
                TreatmentId = 1,
                TimeSlotId = 1,
                Note = "note1",
                BookedDate = DateTime.Parse("2010-09-01"),
                IsDeleted = false
            };
            bool email = book.Email.Contains("@");
            var errorcount = cpv.myValidation(book).Count();
            Xunit.Assert.Equal(0, errorcount);
            Xunit.Assert.True(email);


        }
        //should failed 
        [Fact]
        public void TestBookModel_WithMissingRequeredFeild()
        {
            CheckPropertyValidation cpv = new CheckPropertyValidation();
            var book = new Booking
            {
                Name = "esraa",
                //Email = "esraa.hus@yahoo.com",
                Mobile = "0777768867",
                TreatmentId = 1,
                TimeSlotId = 1,
                Note = "note1",
                BookedDate = DateTime.Parse("2010-09-01"),
                IsDeleted = false
            };
            var errorcount = cpv.myValidation(book).Count();
            Xunit.Assert.Equal(0, errorcount);

        }
        //should failed 
        [Fact]
        public void TestBookModel_WithInvaildDate()
        {
            CheckPropertyValidation cpv = new CheckPropertyValidation();
            var book = new Booking
            {
                Name = "esraa",
                Email = "esraa.hus@yahoo.com",
                Mobile = "0777768867",
                TreatmentId = 1,
                TimeSlotId = 1,
                Note = "note1",
                BookedDate = DateTime.Parse("2018-09-01"),
                IsDeleted = false
            };
            bool date = book.BookedDate <= DateTime.Now.Date;
            var errorcount = cpv.myValidation(book).Count();
            Xunit.Assert.Equal(0, errorcount);
            Xunit.Assert.True(date);

        }

        [Fact]
        public void TestTimeSlotModel()
        {
            CheckPropertyValidation cpv = new CheckPropertyValidation();
            var timeSlot = new LUTimeSlot
            {
                TimeSlotTitle = 1

            };
            var errorcount = cpv.myValidation(timeSlot).Count();
            Xunit.Assert.Equal(0, errorcount);

        }
        [Fact]
        public void TestTreatmentModel()
        {
            CheckPropertyValidation cpv = new CheckPropertyValidation();
            var treatment = new LUTreatment
            {
                TreatmentTitle = "title1",
                TreatmentDescription = "description 1"

            };
            var errorcount = cpv.myValidation(treatment).Count();
            Xunit.Assert.Equal(0, errorcount);

        }


    }
}
